Simple program to solve u,t = -a*u using a parallel in time algorithm. Compile with:

   gfortran -O3 -march=native -fopenmp -c mod_time_int.f90;

   gfortran -O3 -march=native -fopenmp -c odeParTimeOMP.f90;
   
   gfortran -O3 -march=native -fopenmp -o odeParTimeOMP.x *.o;

Program sets number of threads based on how many coarse divisions are requested, since each thread will work on a refined block of data related to coarse division. Outputs coarse and exact solutions to check.dat for a quick sanity check and comparison with the refined model.
