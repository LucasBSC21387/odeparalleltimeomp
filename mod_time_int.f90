module mod_time_int
        contains
                subroutine beuler(dt,uold,unew)

                        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        ! Backwards Euler integration for u,t = -au. Receives time-step size,     !
                        ! u^n and prediction value for u^n+1. Outputs correct value of u^n+1      !
                        ! by means of a Newton-Raphson algorithm.                                 !
                        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                        implicit none

                        real(8), intent(in)    :: dt, uold
                        real(8), intent(inout) :: unew

                        integer(4)             :: iter, maxIter
                        real(8)                :: tol, err
                        real(8)                :: a, r, f, dr, v, T

                        !
                        ! Control params.
                        !
                        maxIter = 100
                        tol = 0.000001d0
                        a = 1.0d0

                        !
                        ! Newton algo.
                        !
                        do iter = 0,maxIter
                           r = -a*unew ! rhs, change if rhs is different
                           dr = -a     ! Derivative dr/dunew, change for different rhs
                           f = unew - dt*r - uold
                           err = sqrt(f*f)
                           if (err .le. tol) then
                              write(*,'("*** Solution converged! ***")')
                              write(*,'("*** Iterations := ",i0)') iter
                              write(*,'("*** Error := ",f16.8)') err
                              exit
                           else
                              T = 1.0d0-dt*dr
                              v = -(1.0d0/T)*f
                              unew = unew+v
                           end if

                        end do
                end subroutine beuler

                subroutine feuler(dt,uold,unew)

                        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        ! Forward Euler integration for u,t = -au. Receives time-step size and    !
                        ! previous value u^n. Outputs new value u^n+1                             !
                        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                        implicit none

                        real(8), intent(in)  :: dt, uold
                        real(8), intent(out) :: unew

                        real(8)              :: a, r

                        !
                        ! Control params.
                        !
                        a = 1.0d0

                        !
                        ! Advance
                        !
                        r = -a*uold
                        unew = uold+dt*r

                end subroutine feuler

end module mod_time_int
