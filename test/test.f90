program test

        use omp_lib

        implicit none

        integer    :: iblock, nblock, iter
        integer(4) :: n, nchunk, nthreads       ! Array and chunk size
        integer(4), allocatable :: a(:), b(:)   ! Full arrays
        integer(4), allocatable :: ac(:), bc(:) ! Array chunks

        !
        ! Define array and chunk size
        !
        n = 2**30
        !nchunk = 2**30
        nchunk = 268435456

        !
        ! Allocate arrays
        !
        allocate(a(n),b(n))
        allocate(ac(nchunk),bc(nchunk))

        !
        ! Define number of threads to call (n/nchunk)
        !
        nblock = n/nchunk
        call omp_set_num_threads(nblock)
        nthreads = omp_get_max_threads()
        write(*,'("There are ",i0," threads being used!")') nthreads

        do iter = 1,10

           !
           ! Initialize arrays
           !
           a(1:n/2) = 1
           a((n/2)+1:n) = 2
           b(1:n/2) = 1
           b((n/2)+1:n) = 2

           !
           ! Sum arrays (parallel blocks)
           !
           !$omp parallel do shared(a,b,nchunk) private(ac,bc,iblock)
           do iblock = 1,nblock
              bc(1:nchunk) = b((iblock-1)*nchunk+1:iblock*nchunk)
              ac(1:nchunk) = a((iblock-1)*nchunk+1:iblock*nchunk)
              ac = ac+bc
              a((iblock-1)*nchunk+1:iblock*nchunk) = ac(1:nchunk)
           end do
           !$omp end parallel do
        end do
        print*, a(1), a(n)

end program test
