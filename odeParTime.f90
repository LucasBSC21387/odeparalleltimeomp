program odeParTime

        use omp_lib
        use mod_time_int

        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        ! Solves the ODE u,t = -au, utilizing a parallel time algorithm. Predicts on a  !
        ! "coarse" time with Backwards Euler, then refines using RK1 on each coarse     !
        ! subsection, with extra divisions. Afterwards, iterate over the newly formed   !
        ! jumps to obtain a fully continuous solution.                                  !
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        implicit none

        integer(4)           :: nt_c, nt_f, itime, jtime, iter
        real(8)              :: t0, u0, dt_c, dt_f
        real(8), allocatable :: t_c(:), u_c(:), u_exact(:)
        real(8), allocatable :: t_f(:), u_f(:), u_reinit(:)
        real(8), allocatable :: tend(:), uend(:)

        !
        ! Input data
        !
        write(*,*) '*** Enter initial time t0:'
        read(*,*) t0
        write(*,*) '*** Enter initial condition u0:'
        read(*,*) u0
        write(*,*) '*** Enter number of coarse steps:'
        read(*,*) nt_c
        write(*,*) '*** Enter coarse time step size:'
        read(*,*) dt_c

        !
        ! Form coarse time grid
        !
        allocate(t_c(nt_c+1))
        t_c(1) = t0
        do itime = 2,nt_c+1
           t_c(itime) = t_c(itime-1)+dt_c
        end do

        !
        ! Exact solution (sanity check)
        !
        allocate(u_exact(nt_c))
        u_exact(1) = u0
        do itime = 2,nt_c+1
           u_exact(itime) = exp(-t_c(itime))
        end do

        !
        ! Initialize coarse solution
        !
        allocate(u_c(nt_c+1))
        u_c = 0.0d0
        u_c(1) = 1.0d0

        !
        ! Compute coarse solution
        !
        do itime = 2,nt_c+1
           call beuler(dt_c,u_c(itime-1),u_c(itime))
           write(*,'("*** itime := ",i0)') itime
           write(*,'("*** u(itime) := ",f16.8)') u_c(itime)
        end do

        !
        ! Write coarse and exact to dump file
        !
        open(unit=99,file="check.dat",status="replace")
        do itime = 1,nt_c+1
           write(99,*) t_c(itime), u_c(itime), u_exact(itime)
        end do
        close(99)

        !
        ! Refine solution (parallel section)
        !
        nt_f = 100000000            ! Time-steps for each chunk
        dt_f = dt_c/dble(nt_f)      ! Refined step size
        allocate(t_f(nt_f+1))       ! Refinedtime step chunk
        allocate(u_f(nt_f+1))       ! Refined solution chunk
        allocate(tend(nt_c*nt_f+1)) ! Total time (nChunks*nFineDivisions+1)
        allocate(uend(nt_c*nt_f+1)) ! Total solution (nChunks*nFineDivisions+1)
        allocate(u_reinit(nt_c+1))  ! Points from where to reinitialize the solution (exclude last one as dummy)

        !
        ! Initialize final dataset
        !
        tend = 0.0d0
        uend = 0.0d0

        !
        ! Initialize reinit. pts. to coarse soln.
        !
        u_reinit(:) = u_c(:)

        !
        ! Iterate over reinitializations (2:nt_c points)
        !
        do iter = 1,nt_c

           !
           ! Modify coarse point with update
           !
           u_c(iter) = u_reinit(iter)

           !
           ! Loop over chunks (parallel OMP)
           !
           do itime = 1,nt_c

              !
              ! Initialize chunk from coarse point
              !
              t_f(1) = t_c(itime)
              u_f(1) = u_c(itime)

              !
              ! Perform integration
              !
              do jtime = 2,nt_f+1
                 t_f(jtime) = t_f(1)+dble(jtime-1)*dt_f
                 call feuler(dt_f,u_f(jtime-1),u_f(jtime))
              end do
              u_reinit(itime+1) = u_f(nt_f+1)

              !
              ! Write chunk to array
              !
              tend((itime-1)*nt_f+1:itime*nt_f+1) = t_f(1:nt_f+1)
              uend((itime-1)*nt_f+1:itime*nt_f+1) = u_f(1:nt_f+1)

           end do
        end do
        STOP 1

        !
        ! Write final to dump file
        !
        open(unit=99,file="final.dat",status="replace")
        do itime = 1,nt_f*nt_c+1
           write(99,*) tend(itime), uend(itime)
        end do
        close(99)

end program odeParTime
